# README #

### What is this repository for? ###

This repo was designed to detect and store any wifi or connection dropouts during a defined period.

### How do I get set up? ###

### HTML Version ###
1st approach: Upload all four files to your domain and run the tool.
2nd approach: Run locally on your laptop, you will need to amend the ajax request to point to any file that is online.
	For example: url:"ANY_LIVE_FILENAME.COM?dynamic=" + theTime.getTime()
	
### PYTHON Version ###
Run dropout.py and it will ping google.com every 5 seconds. It will also create a dropouts.txt file which will log all of your dropouts.
