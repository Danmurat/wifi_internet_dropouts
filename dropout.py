import urllib2
import time
import os

start=0
end=0

while True:
  file = open("dropouts.txt","w")
  theTime = time.ctime()
  def internet_on():
    try:
      nf = urllib2.urlopen("http://www.google.com")
      global start
      start = time.time()

      page = nf.read()

      global end
      end = time.time()

      nf.close()
      return True
    except urllib2.URLError as err: 
      pass

  if internet_on() == True:
    print "All good " + str(end - start)
  else:
    os.system('say "Your Internet Connection Has Dropped"')
    file.write("Internet Connection DOWN!\n\n" + theTime + "\n\n")
    file.close()

  time.sleep(5)