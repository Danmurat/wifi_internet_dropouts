function appendTable(theClass, theStatus){
    var theTime = new Date();
    $('#resTable').prepend("<tr class='" + theClass + "'><td>" + theStatus + "</td><td>" + theTime.getHours() + ":" + (theTime.getMinutes()<10?'0':'') + theTime.getMinutes() + ":" + (theTime.getSeconds()<10?'0':'') + theTime.getSeconds() + "</td></tr>");
}

function ping(){
    var theTime = new Date();
    //var theStatus="";
    //var theClass = "green";
    $.ajax({
        type: 'GET',
        url:"dropout.txt?dynamic=" + theTime.getTime(),
    success:function() {
        theStatus = "Connection active!"; theClass = "green";
        appendTable(theClass, theStatus);
    }, error: function() {
            theStatus = "Connection dropped"; theClass = "red";
            appendTable(theClass, theStatus);
        }
    });
        if($('#resTable tr').length > 20)
        {
            $('table tr.green').remove();
            $('#dropTable').append($('table tr.red'));
        }
}

var letsGo;
function startTest(){
    $('#resTable').html('');
    $('#dropTable').html('');
    window.clearTimeout(letsGo);
    letsGo = window.setInterval(ping, 500);
    var theTime = new Date();
    $('#startTime').text(theTime.getHours() + ":" + (theTime.getMinutes()<10?'0':'') + theTime.getMinutes())
}

function stopTest(){
    window.clearTimeout(letsGo);
}